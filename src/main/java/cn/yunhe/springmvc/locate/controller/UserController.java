package cn.yunhe.springmvc.locate.controller;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping("/user")
public class UserController {

	@RequestMapping("/get/zh")
	public String choiceLang(HttpServletRequest request) {
		request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("zh", "CN"));
		return "user";
	}

	@RequestMapping("/get/en")
	public String showUser(HttpServletRequest request) {
		request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, new Locale("en", "US"));
		return "user";
	}

	@RequestMapping("/get")
	public String showUser() {
		return "user";
	}
}
