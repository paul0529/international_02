package cn.yunhe.springmvc.locate.controller;

import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Controller
@RequestMapping(value = "/util")
public class UtilController {

	/**
	 * 如果language符合中文、英语、马来西亚语，那么就改变，否则使用浏览器默认的语言
	 * 
	 * @param language
	 *            选择的语言
	 * @param rurl
	 *            跳转的url
	 * @return 跳转的url
	 */
	@RequestMapping(value = "/lang/{language}")
	public String changeLanguage(@PathVariable String language, HttpServletRequest request) {
		if (language != null) {
			Locale locale = null;
			if (language.equals("zh_CN")) {
				locale = new Locale("zh", "CN");
			} else if (language.equals("en_US")) {
				locale = new Locale("en", "US");
			} else if (language.equals("ms")) {
				locale = new Locale("ms");
			} else {
				locale = LocaleContextHolder.getLocale();
			}
			request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, locale);
		}
		return "redirect:/user/get";
	}
}
